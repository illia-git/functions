const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') return false;
  if (str1.length === 0) str1 = '0';
  if (str2.length === 0) str2 = '0';
  const REGEX = /^\d+$/;
  if (!REGEX.test(str1) || !REGEX.test(str2)) return false;
  const SUM = parseInt(str1) + parseInt(str2);
  return SUM.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let books = 0,
    comments = 0;

  for (let i in listOfPosts) {
    if (listOfPosts[i]['author'] === authorName) books++;
    for (let j in listOfPosts[i]['comments'])
      if (listOfPosts[i]['comments'][j]['author'] === authorName) comments++;
  }
  
  return `Post:${books},comments:${comments}`;
};

const tickets = (people) => {
  const DEPOSIT = [0, 0, 0];

  const updateDeposit = paid => {
    for (let i in DEPOSIT) DEPOSIT[i] = DEPOSIT[i] + paid[i];
  }

  for (let i in people) {
    if (people[i] === 25) updateDeposit([1, 0, 0]);
    if (people[i] === 50) updateDeposit([-1, 1, 0]);
    if (people[i] === 100) {
      if (DEPOSIT[0] >= 1 && DEPOSIT[1] >= 1) {
        updateDeposit([-1, -1, 1]);
      } else if (DEPOSIT[0] >= 3 && DEPOSIT[1] == 0) {
        updateDeposit([-3, 0, 1]);
      } else {
        updateDeposit([-3, 0, 1]);
      }
    }
  }

  const STATE = (DEPOSIT[0] < 0 || DEPOSIT[1] < 0 || DEPOSIT[2] < 0);
  return STATE ? 'NO' : 'YES';
};

module.exports = {
  getSum,
  getQuantityPostsByAuthor,
  tickets
};
